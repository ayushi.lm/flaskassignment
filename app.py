from flask import Flask, render_template
from datetime import datetime
app = Flask(__name__)


@app.route("/")
def hello_world():
    dt = datetime.now()
    date=dt.strftime("%d")
    n=int(date)%10
    last="th"
    if 13>=int(date)%100>=11 or n>3: last="th"
    elif n==1: last="st"
    elif n==2: last="nd"
    elif n==3: last="rd"
    monthyear = dt.strftime("%b %Y")
    day = dt.strftime("%A")
    time = dt.strftime("%H:%M:%S")
    return render_template("index.html",date = date,last = last,monthyear = monthyear,day=day,time=time)

if __name__=="__main__":
    app.run(debug=True)




    